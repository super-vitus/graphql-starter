import { GraphQLScalarType } from'graphql'
import { get } from 'lodash'

export default {
  Json: new GraphQLScalarType({
    name: 'Json',
    description: 'Json custom scalar type',
    parseValue(value) {
      return value; // value from the client
    },
    serialize(value) {
      return value; // value sent to the client
    },
    parseLiteral(ast) {
      return get(ast,'value') ? null : JSON.parse(get(ast,'value'))
    }
  }),
}
