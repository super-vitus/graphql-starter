import { getUserId, Context } from '../../utils'

export const Query = {
  feed(root, args, context: Context, info) {
    return context.db.query.posts({ where: { isPublished: true } }, info)
  },

  drafts(root, args, context: Context, info) {
    const id = getUserId(context)

    const where = {
      isPublished: false,
      author: {
        id
      }
    }

    return context.db.query.posts({ where }, info)
  },

  post(root, { id }, context: Context, info) {
    return context.db.query.post({ where: { id: id } }, info)
  },

  me(root, args, context: Context, info) {
    const id = getUserId(context)
    return context.db.query.user({ where: { id } }, info)
  },

  debug(root, args, context, info) {
    console.log(context.models)
  },

  async nearbyUsers(root, args, { pg } : Context, info) {
    let result = await pg.client.query(`SELECT * FROM default$default."User"`)
    return result.rows
  }
}
