import { Query } from './Query'
import { auth } from './Mutation/auth'
import { post } from './Mutation/post'
import { AuthPayload } from './AuthPayload'

import scalars from './system/scalars'

const resolvers = {
  Query,
  Mutation: {
    ...auth,
    ...post,
  },
  ...scalars
}

export default resolvers
