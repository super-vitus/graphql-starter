let { context } = require('../dist/main')

context.sequelizeUtils.umzug.up()
.then(r=>{
  console.log(r)
  console.log('migration successful!')
}).catch(e=>{
  console.log('migration error:')
  console.log(e)
}).finally(()=>process.exit(0))