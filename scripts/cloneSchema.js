let { context } = require('../dist')

process.argv.forEach((val, index) => {
  console.log(val);
});

context.sequelize.query(
`CREATE OR REPLACE FUNCTION clone_schema(source_schema text, dest_schema text) RETURNS void AS
$BODY$
DECLARE 
  objeto text;
  buffer text;
BEGIN
    EXECUTE 'CREATE SCHEMA IF NOT EXISTS '"||dest_schema||'"';
    FOR objeto IN
        SELECT TABLE_NAME::text FROM information_schema.TABLES WHERE table_schema = source_schema
    LOOP        
        buffer:= '"'||dest_schema||'"."'||objeto||'"';
        EXECUTE 'CREATE TABLE IF NOT EXISTS '||buffer||' (LIKE "'||source_schema||'"."'||objeto||'" INCLUDING CONSTRAINTS INCLUDING INDEXES INCLUDING DEFAULTS)';
        EXECUTE 'INSERT INTO '||buffer||' (SELECT * FROM "'||source_schema||'"."'||objeto||'")';
    END LOOP;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;`,{
  type: context.sequelize.Sequelize.QueryTypes.SELECT
}).then(()=>{
  console.log('schema replace function successfully created')
}).catch(e=>{
  console.log(e)
}).then(()=>{
  return context.sequelize.query(
    `SELECT clone_schema('default$default','default$test4');`
  ,{
    type: context.sequelize.Sequelize.QueryTypes.SELECT
  }).then(()=>{
    console.log('schema replaced successfully ')
  })
})
