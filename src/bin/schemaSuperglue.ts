// base code here from 'schema glue' by Neap, modified by Devin Despain of Super Vitus Group
import _ from 'lodash'
// import dotenv from 'dotenv'

// dotenv()

export const glue = (modules, options={}) => (context) => {

	const gluedSchema = (modules || []).reduce((a, { schema, resolver, directive, query, mutation, subscription, funcs }) => {
		const s = schema && typeof(schema) == 'string' ? (a.schema + '\n' + schema).trim() : a.schema
		const q = query && typeof(query) == 'string' ? (a.query + '\n' + query).trim() : a.query
		const m = mutation && typeof(mutation) == 'string' ? (a.mutation + '\n' + mutation).trim() : a.mutation
		const sub = subscription && typeof(subscription) == 'string' ? (a.subscription + '\n' + subscription).trim() : a.subscription
		const _funcs = Object.assign({},a.funcs,funcs?funcs(context):{})
		// a.hooks.push(hooks)
    for(let key in resolver) a.resolver[key] = Object.assign((a.resolver[key] || {}), (resolver[key] || {}));
			
		directive = Object.assign((a.directive || {}), (directive || {}));

		return { schema: s, resolver: a.resolver, directive: a.directive, query: q, mutation: m, subscription: sub, funcs : _funcs } 
	}, { schema: '', resolver: {}, directive : {}, query: 'type Query {', mutation: 'type Mutation {', subscription: 'type Subscription {', funcs : {} })

	if (!gluedSchema.schema) {
			/*eslint-disable */
			throw new Error(`Missing GraphQL Schema: No schemas found in included modules'`)
			/*eslint-enable */
	}

	if (gluedSchema.query != 'type Query {') {
		gluedSchema.query = gluedSchema.query + '\n}'
		gluedSchema.schema = gluedSchema.schema + '\n' + gluedSchema.query
	}
	if (gluedSchema.mutation != 'type Mutation {') {
		gluedSchema.mutation = gluedSchema.mutation + '\n}'
		gluedSchema.schema = gluedSchema.schema + '\n' + gluedSchema.mutation
	}
	if (gluedSchema.subscription != 'type Subscription {') {
		gluedSchema.subscription = gluedSchema.subscription + '\n}'
		gluedSchema.schema = gluedSchema.schema + '\n' + gluedSchema.subscription
	}
	
	const _context = Object.assign({ funcs : gluedSchema.funcs }, context)

	// const _hooks = _.reduce(gluedSchema.hooks,(mem,hook)=>Object.assign(hook(_context),mem),{})
	
	return Object.assign({},{ context : _context },{ 
    schema: gluedSchema.schema, 
		resolver: gluedSchema.resolver, 
		directiveResolvers : gluedSchema.directive,
		// hooks : _hooks,
  })
}
