const path = require('path')
const fs = require('fs')
const glob = require('glob')
const _ = require('lodash')
const SequelizeFixtures = require('sequelize-fixtures')

const importModel = (name, modelModules, defaultTableOpts, context) => {
	const gluedSchema = _.reduce(modelModules || [],(a, { attrs, opts, instanceMethods, classMethods }) => {
    a.instanceMethods.push(instanceMethods?instanceMethods:()=>({}))
    a.classMethods.push(classMethods?classMethods:()=>({}))
    return { 
      attrs : Object.assign({}, typeof attrs == 'function' ? attrs(context) : attrs || {} ,a.attrs), 
      opts : Object.assign({}, typeof opts == 'function' ? opts(context) : opts || {} ,a.opts), 
      instanceMethods : a.instanceMethods, 
      classMethods : a.classMethods
    }
  }, { 
    attrs : {}, 
    opts : defaultTableOpts, 
    instanceMethods : [], 
    classMethods : []
  })
  return {
    modelDef : (Seq)=>Seq.define(
      name,
      gluedSchema.attrs,
      _.defaults(gluedSchema.opts, { tableName: name })
    ),
    instanceMethods : gluedSchema.instanceMethods,
    classMethods : gluedSchema.classMethods
  }
}


export default ({ modelModules, defaultTableOpts, context, sequelize, })=>{
  let models = {}
  let modelConfig = {}
  
  _.map(modelModules,(modelModuleGroup, name)=>{
      modelConfig[name] = importModel(name, modelModuleGroup, defaultTableOpts, context)
      let newModel = modelConfig[name].modelDef(sequelize)
      models[name] = newModel
  })

  _.keys(models).forEach(function (modelName) {
  
    _.map(modelConfig[modelName].instanceMethods,methodFunc=>{
      const instanceMethods = methodFunc(Object.assign({},context,{models}))
      return _.mapValues(instanceMethods,(method,methodName)=>{
        models[modelName].prototype[methodName] = method
      })
    })
  
    _.map(modelConfig[modelName].classMethods,methodFunc=>
      _.mapValues(methodFunc(Object.assign({},context,{models})),(method,methodName)=>{
        models[modelName][methodName] = method
      })
    )
      
    if ('associate' in models[modelName]) {
      if(process.env.NODE_ENV !== 'test'){
        console.log('building associations for Sequelize model ' + modelName, 'info')
      }
      models[modelName] = models[modelName].associate(models)
    }
  })

  sequelize.loadFixtures = (path, env)=>{
    return SequelizeFixtures.loadFile(path, models, { log : (r)=>{ env=='test' ? null : console.log(r) } })
  }

  return {
    sequelize,
    models
  }
}
