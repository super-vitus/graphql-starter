import { GraphQLServer } from 'graphql-yoga'
// import { Engine } from 'apollo-engine'

import { Prisma } from './generated/prisma'
// import resolvers from './resolvers'
import _context from './context'

import _schema from './graphql'

const server = new GraphQLServer({
  // typeDefs: './src/schema.graphql',
  // resolvers,
  schema : _schema,
  context: req => ({
    ...req,
    ..._context,
    db: new Prisma({
      endpoint: process.env.PRISMA_ENDPOINT, // the endpoint of the Prisma API (value set in `.env`)
      debug: true, // log all GraphQL queries & mutations sent to the Prisma API
      // secret: process.env.PRISMA_SECRET, // only needed if specified in `database/prisma.yml` (value set in `.env`)
    }),
  }),
})
server.start(() => console.log(`Server is running on http://localhost:4000`))

export const context = _context