import {GraphQLSchema} from 'graphql';
import { makeExecutableSchema } from 'graphql-tools';
import 'apollo-cache-control';
import * as _ from 'lodash'
import { glue } from '../bin/schemaSuperglue'
import _context from '../context'

import * as Tasks from './tasks'
import * as SchemaTypes from './schemaTypes'

const { schema, resolver, context, directiveResolvers } = glue([
  Tasks,
  SchemaTypes,
])(_context)

const executableSchema: GraphQLSchema = makeExecutableSchema({
  resolvers : resolver,
  typeDefs : schema,
  directiveResolvers : _.extend(directiveResolvers)
});

export default executableSchema;

export const directives = directiveResolvers
