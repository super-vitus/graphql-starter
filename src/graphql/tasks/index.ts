import * as _ from 'lodash'
import Bluebird from 'bluebird'
// import { throwIfNotAdmin } from './scope'

export const schema = `
type Queue {
  name : ID!
  getJob(id:ID!) : Json
  getJobs(types:[String!],start:Int,end:Int,asc:Boolean) : Json
  getFailed(start:Int,end:Int):Json
  getActive(start:Int,end:Int):Json
  getDelayed(start:Int,end:Int):Json
  getCompleted(start:Int,end:Int):Json
  getWaiting(start:Int,end:Int):Json
  getRepeatableJobs : Json
  count : Json
  jobCounts : Json
}
`

export const query = `
  getQueues:[Queue]
  getQueue(name:ID!):Queue
  getJob(name:ID!,id:ID!):Json
`

const Query = {
	async getQueues(root,args,{ tasks, session }){
    // throwIfNotAdmin(session)
    return _.map(tasks.queues,({ name })=>({ name }))
	},
	async getQueue(root,{ name },{ tasks, session }){
    // throwIfNotAdmin(session)
    return { name }
	},
}

export const mutation = `
  clearRepeatableTasks(name:ID!):Json
  pauseQueue(name:ID!,isLocal:Boolean):Json
  resumeQueue(name:ID!,isLocal:Boolean):Json
  emptyQueue(name:ID!):Json
  cleanQueue(name:ID!,grace:Int!,status:String,limit:Int):Json
  removeRepeatableJob(name:ID!,scheduleId:ID!):Json
`
const Mutation = {
  async pauseQueue(root,{ name, isLocal },{ session, tasks }){
    // throwIfNotAdmin(session)
    let queue = tasks.queues[name]
    return queue.pause(isLocal)
  },
  async resumeQueue(root,{ name, isLocal },{ session, tasks }){
    // throwIfNotAdmin(session)
    let queue = tasks.queues[name]
    return queue.pause(isLocal)
  },
  async emptyQueue(root,{ name },{ session, tasks }){
    // throwIfNotAdmin(session)
    let queue = tasks.queues[name]
    return queue.empty()
  },
  async clearRepeatableTasks(root,{ name },{ session, models, tasks }){
    // throwIfNotAdmin(session)
    let queue = tasks.queues[name]
    let jobs = await queue.getRepeatableJobs();
    return Bluebird.map(jobs,({ name, tz, cron, endDate, limit })=>{
      return queue.removeRepeatable({ tz, cron, endDate, limit })
    })
  },
  async cleanQueue(root,{ name, grace, status, limit },{ session, models, tasks }){
    // throwIfNotAdmin(session)
    let queue = tasks.queues[name]
    return queue.clean(grace, status, limit)
  },
  async removeRepeatableJob(root,{ name, scheduleId },{ tasks, session }){
    // throwIfNotAdmin(session)
    let queue = tasks.queues[name]
    let repeat = JSON.parse(Buffer.from(scheduleId, 'base64').toString('ascii'))
    return queue.removeRepeatable(repeat)
  }
}

export const resolver = {
  Query,
  Mutation,
  Queue : {
    async getJob({ name },{ id },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getJob(id)
    },
    async getJobs({ name },{ types, start, end, asc },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      let jobs = await tasks.queues[name].getJobs(types, start, end, asc); 
      return _.sortBy(jobs,'finishedOn')
    },
    async getRepeatableJobs({ name },args,{ tasks, session }){ 
      // throwIfNotAdmin(session)
      let jobs = await tasks.queues[name].getRepeatableJobs(); 
      return _.map(jobs,job=>Object.assign({},job,{
        scheduleId : Buffer.from(JSON.stringify({ 
          cron : job.cron,
          tz : job.tz,
          endDate : job.endDate,
          limit : job.limit,
        })).toString('base64')
      }))
    },
    async getFailed({ name },{ start, end },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getFailed(start,end); 
    },
    async getActive({ name },{ start, end },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getActive(start,end); 
    },
    async getDelayed({ name },{ start, end },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getDelayed(start,end); 
    },
    async getCompleted({ name },{ start, end },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getCompleted(start,end); 
    },
    async getWaiting({ name },{ start, end },{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getWaiting(start,end); 
    },
    async count({ name },args,{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].count()
    },
    async jobCounts({ name },args,{ tasks, session }){ 
      // throwIfNotAdmin(session)
      return tasks.queues[name].getJobCounts()
    },
  }

}