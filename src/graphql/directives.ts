// const endActiveCampaigns = require('./endActiveCampaigns')
import * as _ from 'lodash'

export const schema = `
  directive @admin on FIELD_DEFINITION
`

export const directive = {
  admin(next,src,args,{ session },{ fieldName, parentType }){
    return next().then(r=>{
      if(!_.includes(session.scopes,'admin')) throw new Error(`user does not have correct permissions to query field ${fieldName} on type ${parentType} of id ${src.id}`);
      return r
    })
  },
}