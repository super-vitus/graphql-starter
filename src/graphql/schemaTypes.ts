// import GraphQLJSON from'graphql-type-json'
import { GraphQLScalarType } from'graphql'
import * as moment from 'moment'
import * as _ from 'lodash'

export const schema = `
  scalar Json

  type JsonMutationResponse {
    result : Json
    error : Json
  }

  input SearchInputParams {
    search : String,
    searchFields : [String!]!
  }

  input ListInputParams {
    where : Json,
    offset : Int,
    limit : Int,
    order : Json,
    scope : String,
  }

  input WithinInputParams {
    lat : Float,
    lng : Float,
    distance : Int,
  }

  input PatchInput {
    op : String!,
    path : String!,
    value : Json
  }

  input UploadBody {
    key : String!,
    size : Int!,
    filetype : String!,
  }

  type UploadResponse {
    url : String,
    signedUrl : String,
    libraryItem : Json
  }

`
export const resolver = {

  Json: new GraphQLScalarType({
    name: 'Json',
    description: 'Json custom scalar type',
    parseValue(value) {
      return value; // value from the client
    },
    serialize(value) {
      return value; // value sent to the client
    },
    parseLiteral(ast) {
      return _.get(ast,'value') ? null : JSON.parse(_.get(ast,'value'))
    }
  }),

}
