const Queue = require('bull')
import { toArray, reduce } from 'lodash'
import config from '../config'
let host = `redis://${config.redis.connection.host}:${config.redis.connection.port}`
let queues = reduce(config.queues,(mem,val,key)=>{
  if(!val.disabled){ 
    mem[key] = new Queue(key, host)
    if(val.paused){
      mem[key].pause().then(()=>{
        console.log(`bull task queue ${key} was created and paused`)
      })
    }else{
      console.log(`bull task queue ${key} was created`)
    }
  }else{
    console.log(`bull task queue ${key} was disabled`)
  }
  return mem
},{})

export default {
  Queue,
  queues
}