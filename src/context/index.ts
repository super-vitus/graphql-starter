import config from '../config'
import makeSequelizeWithContext from '../sequelize'

import pg from './pg'
import tasks from './tasks'
import { v4 } from 'node-uuid'

// const pubsub = new RedisPubSub(config.redis)

export interface ContextInterface {
  config : object,
  pg : object,
  v4 : any,
  sequelize : object,
  models : object,
}

let context = {
  config,
  pg,
  v4,
  tasks,
}

let { sequelize, models } = makeSequelizeWithContext(context)

context['sequelize'] = sequelize
context['models'] = models

export default context