const { Client, Pool } = require('pg')
import config from '../config'
const client = new Client(config.postgres)
const pool = new Pool(config.postgres)

pool.connect((err, client, done) => {
  if (err) throw err
  client.query('SET search_path TO default$default', (err, res) => {
    if (err) {
      console.log(err.stack)
    }
  })
})

// console.log(client)
export default {
  client : pool
}