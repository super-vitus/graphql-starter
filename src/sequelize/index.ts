import Sequelize from 'sequelize'
import sequelizeSuperglue from '../bin/sequelizeSuperglue'
import * as wkx from 'wkx'
import * as DataTypes from 'sequelize/lib/data-types'
import * as _ from 'lodash'

import * as AppUser from './AppUser'
import * as AppUserClassMethods from './AppUser/classMethods'
import * as AppUserInstanceMethods from './AppUser/instanceMethods'

// console.log(DataTypes)
// GCE Cloud SQL doesn't play nicely with ST_GeomFromGeoJSON 
// so these two methods must be modified to compensate. These
// should work fine on a non-GCE instance as well. 
DataTypes.postgres.GEOMETRY.prototype._stringify = function _stringify(value, options) {
  return `ST_GeometryFromText('${wkx.Geometry.parseGeoJSON(value).toWkt()}')`
};
DataTypes.postgres.GEOMETRY.prototype.parse = function _stringify(value, options) {
  return _.get(wkx.Geometry.parse(value).toGeoJSON(),'coordinates')
};

export default function makeSequelizeWithContext(context){
  
  context.sequelize = new Sequelize(context.config.postgres);
  
  const { models, sequelize } = sequelizeSuperglue({
    modelModules : {
      AppUser : [ AppUser, AppUserClassMethods, AppUserInstanceMethods ],
    },
    defaultTableOpts : {
      paranoid: true,
      underscored: true,
      // schema : context.config.postgres.schema
    },
    context,
    sequelize : context.sequelize
  })

  if(context.config.env !== 'test'){
    sequelize.sync()
  }

  return {
    sequelize,
    models
  }
  
}