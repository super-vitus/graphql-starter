import Sequelize from 'sequelize'
const { BIGINT, STRING, DATE, BOOLEAN, INTEGER, FLOAT, ARRAY, JSONB, GEOMETRY, VIRTUAL, TEXT, UUID, UUIDV4, ENUM } = Sequelize
import makeJsonStore from '../_utils/makeJsonStore'

import { cmsDefaults } from './definitions'

export const attrs = ({ v4 })=>({
  
  id : {
    type : UUID,
    primaryKey : true,
    defaultValue: ()=>v4()
  },

  scope : {
    type : ENUM('admin','agent','viewer','user','public')
  },

  coords : GEOMETRY,
  distance : VIRTUAL,

  cms_store : makeJsonStore(cmsDefaults,'cms_store'),

  search : TEXT,

})

export const opts = {
  scopes : {
    all_including_archived : {
      where : {}
    }
  },
}