import { v4 } from 'node-uuid'
import * as moment from 'moment'

export const cmsDefaults = ()=>({
  
  profile : {
    title : '',
    added_at : new Date(),
    email : '',
    first_name : '',
    last_name : '',
  },
  
  location : {
    locality : null,
    address : null,
    google_place_id : null,
    google_place_data : {}
  },
  
  metadata : {
    added_at : new Date(),
  },
  
  
})