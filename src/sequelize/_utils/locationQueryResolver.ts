import sequelize from 'sequelize'
import * as _ from 'lodash'

export default function(args, model) {
  const attributes = { include : [] }
  
  if(args.where){
    args.where = JSON.parse(args.where)
  }else{
    args.where = {}
  }

  if(args.within){
    
    try{
      args.within = JSON.parse(args.within)
    }catch(e){
      // const attributes = null 
    }

    const withinCoords = sequelize.fn('ST_GeographyFromText', `SRID=4326;POINT(${args.within.lng} ${args.within.lat})`)

    attributes.include.push([sequelize.fn('ST_DISTANCE', sequelize.col('coords'), withinCoords),'distance'])

    args.where = {
      $and : [
        { coords : { $ne : null }},
        sequelize.fn(
          'ST_DWithin', 
          sequelize.col('coords'),
          withinCoords,
          args.within.distance/0.000621371 
        ),
        args.where
      ]
    }

    args.order = args.order || 'distance'

  }else{
    const attributes = null 
  }

  return model.findAll({
    attributes,
    where : args.where,
    offset : args.offset,
    limit : args.limit,
    order: args.order
  }).map(r=>{

    if(r.dataValues.distance){
      r.dataValues.distance = _.get(r,'dataValues.distance')*0.000621371;
    }
    
    return r
  })

}
  