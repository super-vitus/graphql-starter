import * as _ from 'lodash'

const makeSearchHook = (keys)=>(instance, options)=>{
  instance.setDataValue('search',_.map(keys,key=>{
    return _.get(instance,key)
  }).join(' '))
}

export default makeSearchHook