import * as _ from 'lodash'

export default (key:string,defaults:any,mask?:[string])=>function(val){
  console.log(key,defaults,this.dataValues.cms)
  return this.setDataValue(key,_.extend(defaults,this.dataValues[key],_.pick(val,mask ? mask : _.keys(defaults))))
}