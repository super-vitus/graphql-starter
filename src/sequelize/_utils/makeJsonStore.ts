import Sequelize from 'sequelize'
import _ from 'lodash'

const { BIGINT, STRING, DATE, BOOLEAN, INTEGER, FLOAT, ARRAY, JSONB, GEOMETRY, VIRTUAL, TEXT, UUID, UUIDV4, ENUM } = Sequelize

export default (cmsDefaults,attribute)=>({
  type : JSONB,
  defaultValue : cmsDefaults,
  set(val){ 
    let currentValues = this.getDataValue(attribute)
    let defaults = cmsDefaults()
    let store = currentValues ? currentValues : defaults
    _.map(_.keys(defaults),key=>{ 
      val[key] = val[key] ? _.pick(val[key],_.keys(defaults[key])) : null
      if(val[key]) _.set(store,key,_.extend(store[key],val[key]));
      _.set(store,key,_.defaults(store[key],defaults[key]) )
    })
    this.setDataValue(attribute,store)
  }
})