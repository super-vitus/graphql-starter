

export const defaults = {
  postgres: () => ({
    host: 'localhost',
    port : 5444,
    user : 'prisma',
    username : 'prisma',
    password : 'prisma',
    database : 'prisma',
    dialect: 'postgres',
    pool: {
      max: 4,
      min: 1,
      acquire: 30000,
      idle: 10000,
      evict : 5000,
    },
    logging : false,
    operatorsAliases: true,
    schema : `default$${process.env.NODE_ENV||'default'}`,
    // autoSyncAlter : true,
    // autoMigrate : false,
  })
}

// export const test = {
//   postgres: () => {
//     return  {
//       'database': 'localfluence_test',
//       'host': 'localhost',
//       'username': 'postgres',
//       'password': 'docker',
//       // 'autoMigrate': true,
//       'dialect': 'postgres',
//       'port': 5431,
//       'logging': false,
//       // 'loadFixtures': true,
//       prefix: ''
//     }
//   }
// }

// export const production = {
//   postgres: () => ({
//     username : 'proxyuser',
//     user : 'proxyuser',
//     password : '',
//     database : 'postgres',
//     port : 5432,
//     host: '0.0.0.0',
//     dialect: 'postgres',
//     // modelPaths : ['src/postgres/**/index.ts'],
//     pool: {
//       max: 10,
//       min: 1,
//       acquire: 10000,
//       idle: 10000,
//       evict : 5000,
//       ssl : true,
//     },
//     ssl : true,
//     logging : false,
//     operatorsAliases: true,
//     // autoSyncAlter : true,
//     autoMigrate : false,
//   })
// }
  

// export const development = {
//   postgres: () => {
//     // return {
//     //   // 'autoMigrate': true,
//     //   // 'loadFixtures': false,
//     //   'database': process.env.POSTGRES_DEV_DB,
//     //   'host': process.env.POSTGRES_DEV_HOST,
//     //   'username': process.env.POSTGRES_DEV_USER,
//     //   'password': process.env.POSTGRES_DEV_PASS,
//     //   'dialect': 'postgres',
//     //   'port': process.env.POSTGRES_DEV_PORT,
//     //   'logging': false,
//     //   'prefix': ''
//     // }
//   }
// }