
export const defaults = {
  queues: (api) => ({
    consoleHeartbeat : { paused : true }
  })
}

export const test = {
  queues: (api) => ({
    consoleHeartbeat : { paused : true }
  })
}

export const production = {
  queues: (api) => ({
    consoleHeartbeat : {}
  })
}
