
export const defaults = {
  redis: (api) => ({
    connection : {
      host : 'localhost',
      port : 6379,
      db : 0,
      retry_strategy: options => {
        return Math.max(options.attempt * 100, 3000);
      }
    }
  })
}

export const test = {
  redis: (api) => ({
    connection : {
      host : process.env.TEST_REDIS_HOST || 'localhost',
      port : 6379,
      db : 2,
      retry_strategy: options => {
        // reconnect after upto 3000 milis
        return Math.max(options.attempt * 100, 3000);
      }
    }
  })
}

export const production = {
  redis: (api) => ({
    connection : {
      host : process.env.REDIS_HOST || 'localhost',
      port : 6379,
      db : 1,
      retry_strategy: options => {
        // reconnect after upto 3000 milis
        return Math.max(options.attempt * 100, 3000);
      }
    }
  })
}
