import makeConfig from '../bin/makeConfig'

import * as Postgres from './postgres'
import * as Redis from './redis'
import * as Tasks from './tasks'

const configFiles = [
  Postgres,
  Redis,
  Tasks,
]

let env = 'development'

if (process.env.NODE_ENV) {
  env = process.env.NODE_ENV
}

export default makeConfig(configFiles,env)
