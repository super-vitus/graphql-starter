<!-- ## Requirements

You need to have the [GraphQL CLI](https://github.com/graphql-cli/graphql-cli) installed to bootstrap your GraphQL server using `graphql create`:

```sh
npm install -g graphql-cli
``` -->

## Initializing

```sh
# in the project root
yarn install
prisma deploy
```

## Running 

```sh
# in the project root
yarn dev
```
